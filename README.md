# Methods in Molecular Biology: Stacks 2 Protocol

Supplementary information for:

> Rivera-Col�n, A.G., Catchen, J.M. (2022). **Population Genomics Analysis with RAD, Reprised: Stacks 2**. In: Verde, C., Giordano, D. (eds) *Marine Genomics. Methods in Molecular Biology*, vol 2498. Humana, New York, NY. [DOI: 10.1007/978-1-0716-2313-8_7](https://doi.org/10.1007/978-1-0716-2313-8_7)

(C) 2022 Angel G. Rivera-Colon & Julian M. Catchen

## Data

Data files required for analysis:

### Reference genome

```sh
e_maclovinus.genome.fasta.gz
```

Genome assembly for the Patagonian Blennie (*Eleginops maclovinus*) in FASTA format. The assembly contains a total of 628 sequences, including 24 chromosome-level contigs and smaller unplaced scaffolds, for a total size of 613Mb and a scaffold N50 of 26.7 Mb.

### Barcodes

```sh
barcodes.tsv
```

Barcodes file used to demultiplex and name samples in `process_radtags`. Tab-separated file containing two columns: 1) 7bp barcode and 2) sample prefix.

```sh
ACTAGGA<tab>emac_va_01
CCGATAG     emac_va_02
TACATTG     emac_va_03
GGCGGAG     emac_va_04
ACTACTT     emac_va_05
```

For more information on the structure of the barcodes file please refer to the [Stacks Manual](https://catchenlab.life.illinois.edu/stacks/manual/#specbc).

### Popmap files

A population map (Popmap) is a tab-separated file containing two columns: 1) sample prefix of each individual in the analysis, and 2) an integer or string specifying the the population.

```sh
emac_va_01<tab>va
emac_va_02     va
emac_va_03     va
emac_pn_04     pn
emac_pn_05     pn
emac_pn_06     pn
```

For more information on the structure of the Popmap file please refer to the [Stacks Manual](https://catchenlab.life.illinois.edu/stacks/manual/#popmap).

This repository contains three Popmap files:

| File | Description |
| ---- | ----------- |
| `popmap.tsv` | Popmap containing all 60 samples across two sampling location (`va` and `pn`), i.e., all samples. |
| `popmap_opt.tsv` | Popmap containing 20 samples (10 from each location) used for parameter optimization. Uses the `opt` population identifier for all 20 samples. |
| `popmap.rm_low_cov.tsv` | Popmap contining 56 samples across the two sampling locations. It excludes 4 low-coverage samples. |

### Raw Reads

Because of their size, the raw read FASTQ files cannot be stored in this repository. Instead, we are providing a test version of the data here:

```sh
EmacRADSubsample_NoIndex_L001_R1_001.fastq.gz
EmacRADSubsample_NoIndex_L001_R2_001.fastq.gz
```

These files are composed of a subsample of ~10M read pairs originating form the original raw FASTQ read files. Each FASTQ is about 700 Mb in size. These subsampled reads are just intended for testing purposes. Note that, when running the procotcol using these reads, the behavior of the overall pipeline will be different from the full protocol, i.e., fewer-overall reads per-sample in `process_radtags`, significant reduction in assembled loci and coverage in the *de novo* pipeline, etc.

For access to the whole dataset, please visit the associated [DRYAD repository](https://doi.org/10.5061/dryad.jq2bvq8ch).

## Supplementary scripts

### Plot library demultiplexing in processing

The file `01_process_radtags/process_radtags_stats.R` uses the log reported by `process_radtags` and calculates several summary statistics on the proportion of samples in the library, number of total reads, and percentage of total reads kept per-sample. It also generates several plots showing the generated distributions.

The input file can be obtained using:

```sh
stacks-dist-extract process_radtags.raw.log per_barcode_raw_read_counts > per_sample_counts.tsv
```

### Parameter optimization shell loop

The file `02_param_opt/param_opt_loop.sh` loops over the values of 1 through 12, and corresponding number in each iteration as the corresponding value for `ustacks -M` for the Stacks *de novo* pipeline.

### `gstacks` coverage and PCR duplicates

The file `03_denovo/gstacks_stats.R` uses the log reported by `gstacks` and calculates several summary statistics on sample coverage and PCR duplicates. It also generates several plots showing the generated distributions.

The input file can be obtained using:

```sh
stacks-dist-extract gstacks.log.distribs effective_coverages_per_sample | grep -v '^#' > effective_coverages_per_sample.tsv
```

### Per-sample read alignment shell loop

The file `04_refmap/bwa_samples_loop.sh` loops over each sample in the provided population map file, and for each sample aligns the paired reads with `bwa mem` and processed alignments using `samtools view/sort`. For each iteration of the loop, the sample name present in the popmap is used as the prefix from which the path the input reads and output BAM files are constructued.

## Authors

**Angel G. Rivera-Colon**  
Department of Evolution, Ecology, and Behavior  
University of Illinois at Urbana-Champaign  
<angelgr2@illinois.edu>

**Julian M. Catchen**  
Department of Evolution, Ecology, and Behavior  
University of Illinois at Urbana-Champaign  
<jcatchen@illinois.edu>
