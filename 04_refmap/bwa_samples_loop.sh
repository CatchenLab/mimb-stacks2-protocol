#!/bin/bash

# Absolute path to the stacks 2 protocol directory
work=/path/to/stacks2-protocol

# Create BWA database
bwa index $work/04_refmap/genome/e_maclovinus.genome.fasta $work/04_refmap/genome/emac_db

# Open the popmap file, and loop over the samples with a `while` loop
cat $work/04_refmap/info/popmap.tsv | cut -f 1 |
while read sample; do
    # Create variables for each sample
    fq1=$work/01_process_radtags/samples/${sample}.1.fq.gz # Forward reads
    fq2=$work/01_process_radtags/samples/${sample}.2.fq.gz # Reverse reads
    bam=$work/04_refmap/aligned_samples/${sample}.bam      # BAM output
    # Align reads and process alignments
    bwa mem $work/04_refmap/genome/emac_db $fq1 $fq2 | \   # Aling with BWA mem
        samtools view -b -h | \                            # Compress alignments
        samtools sort -o $bam                              # Sort and save to BAM
done
