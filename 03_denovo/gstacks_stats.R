#!/usr/bin/env Rscript

# Plot Stacks-gstacks per-sample statistics

## Input data

### Path to store PDFs
pdfPath <- '/path/to/03_denovo/denovo.M3.rm_pcr_dups'

### Path to the gstacks log `effective_coverages_per_sample.tsv`
covsPath <- '03_denovo/denovo.M3.rm_pcr_dups/effective_coverages_per_sample.tsv'

### Read input file as dataframe
covs  <- read.delim(covsPath, comment.char = '#')

### Add population identifier
covs$popID <- sapply(strsplit(covs$sample, '_'),
                     function(covs){covs[2]})

## Summary Stats
avg_cov <- mean(covs$mean_cov_ns)    # 31.7X
avg_dup <- mean(covs$pcr_dupl_rate)  # 54.7%

## Plot

### Initialize PDf
pdf(paste(pdfPath, '/gstacks_covs_pcr.pdf', sep=''), width=10, height=10)

### A. Coverage

#### Sort the dataframe by the coverage
covs <- covs[order(covs$mean_cov_ns),]

#### Plot dotchart
dotchart(covs$mean_cov_ns,
         labels=covs$sample,
         xlab='',
         xaxt='n',
         xlim=c(0,65))
axis(1,at=seq(0,65,10),labels=seq(0,65,10))
title(xlab='Coverage', line=2)
abline(v=mean(covs$mean_cov_ns), lty=2, col='#b22222')

#### Plot per-population
boxplot(mean_cov_ns~popID, data=covs,
        las=1, 
        xlab='',
        ylab='',
        xaxt='n',
        col=c("#E69F00", "#56B4E9"),
        horizontal = TRUE,
        ylim=c(0,65))
axis(1,at=seq(0,65,10),labels=seq(0,65,10))
title(xlab='Coverage', line=2)
title(ylab='Population', line=2)

### B. PCR Dups

#### Sort the dataframe by the coverage
covs <- covs[order(covs$pcr_dupl_rate),]

#### Plot dotchart
dotchart(covs$pcr_dupl_rate,
         labels=covs$sample,
         xlab='',
         xaxt='n',
         xlim=c(0.4,0.6))
axis(1,at=seq(0.4,0.6,0.05),labels=seq(0.4,0.6,0.05))
title(xlab='PCR Duplicate Rate', line=2)
abline(v=mean(covs$pcr_dupl_rate), lty=2, col='#b22222')

#### Plot per-population Boxplot
boxplot(pcr_dupl_rate~popID, data=covs,
        las=1, 
        xlab='',
        ylab='',
        xaxt='n',
        col=c("#E69F00", "#56B4E9"),
        horizontal = TRUE,
        ylim=c(0.4,0.6))
axis(1,at=seq(0.4,0.6,0.05),labels=seq(0.4,0.6,0.05))
title(xlab='PCR Duplicate Rate', line=2)
title(ylab='Population', line=2)

.=dev.off()
