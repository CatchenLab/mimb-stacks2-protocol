#!/bin/bash

# Absolute path to the stacks 2 protocol directory
work=/path/to/stacks2-protocol

# Loop over the M values
# Iterates over every value between 1 and 12
for M in {1..12}; do
    # This creates a new output directory per Stacks run
    out=$work/02_param_opt/denovo.M${M}
    mkdir -p $out
    # Move into the new directory
    cd $out
    # Stacks command
    denovo_map.pl \
        --samples $work/01_process_radtags/samples \   # Input reads
        --popmap $work/02_param_opt/info/popmap.tsv \  # Population map
        --out-path $out \                              # Output directory (made above)
        --paired \                                     # Use paired reads
        -M $M \                                        # Ustacks M (loop variable)
        -n $M \                                        # Cstacks n (loop variable)
        -T 12 \                                        # Use 12 threads
        --min-samples-per-pop 0.8 \                    # Keep R80 loci
        --rm-pcr-duplicates                            # Remove PCR duplicates
done
